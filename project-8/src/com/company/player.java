package com.company;

public class player {


    //Refactor name: we can change name of any var throughout the .java using refactor. Right click the var and click refactor. This method preserves the get<old name> and they still work.

    /*
     * here I changed:
     * private int hp;
     * to
     * private int hp;
     */
    private String name;
    private int hp;


    //Constructor
    public player(String name, int hp) {
        this.name = name;

        //This is encapsulation. When using constructor, we can only enter values b/w 1 to 100.
        if (hp > 0 && hp <= 100) {
            this.hp = hp;
        } else {
            System.out.println("Invalid hp. Enter a value between 1 and 100.");
        }
    }


    //Method: damage
    public void lose_health(int damage) {
        hp = hp - damage;
        if (hp <= 0) {
            System.out.println("You Died");
        }
    }

    //Setter and getter
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        //Encapsulation
        if (hp <= 100) {
            this.hp = hp;
        }
    }
}
