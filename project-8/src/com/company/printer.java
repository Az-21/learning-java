package com.company;

public class printer {

    private int pages_printed;
    private boolean duplex;
    private float toner;


    //Constructor
    public printer(int pages_printed, boolean duplex, float toner) {
        this.pages_printed = pages_printed;
        this.duplex = duplex;

        if (toner > 0.0 && toner <= 100.0) {
            this.toner = toner;
        } else {
            System.out.println("Please enter the toner level. It ranges between 0 to 100%.");
        }
    }


    //Setter and Getter
    public int getPages_printed() {
        return pages_printed;
    }

    public void setPages_printed(int pages_printed) {
        this.pages_printed = pages_printed;
    }

    public boolean isDuplex() {
        return duplex;
    }

    public void setDuplex(boolean duplex) {
        this.duplex = duplex;
    }

    public float getToner() {
        return toner;
    }

    public void setToner(float toner) {
        this.toner = toner;
    }


    //Methods

    public void print(int sides) {

        //for non duplex printer
        if (duplex == false) {

            //toner consumed (0.04% per side)
            if ((float) toner - 0.04 * sides >= (float) 0) {
                toner = (float) (toner - 0.04 * sides);

                pages_printed = pages_printed + sides;
                System.out.println(sides + " page(s) printed.");
            } else {
                System.out.println("Insufficient ink. Please reduce the number of pages or refill toner.");
            }




        } else if (duplex == true) {

            //toner consumed (0.04% per side)
            if ((float) toner - 0.04 * sides >= 0) {
                toner = (float) (toner - 0.04 * sides);

                pages_printed = pages_printed + sides / 2;
                System.out.println(sides / 2 + " page(s) printed.");
            }
            else {
                System.out.println("Insufficient ink. Please reduce the number of pages or refill toner.");
            }


            System.out.println("Your toner level is: " + toner + "%");
        }



    }

}
