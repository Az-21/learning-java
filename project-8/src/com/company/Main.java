package com.company;

public class Main {

    public static void main(String[] args) {

        //Making fields like health public can lead to misuse of these fields. It can also lead to problems when we change the var name in a sub-class. To counter it, we use encapsulation.

        player player1 = new player("Player 1", 99);

        System.out.println(player1.getHp());



        /*--------------------------------
        Encapsulation challenge
        ----------------------------------*/

        printer hp = new printer(0, true, 90);

        hp.print(100);





    }
}
