package com.az21;

public class Main_class {

    //Object Oriented Programming
    public static void main(String[] args) {

        /*-----------------------------
        Classes
        ------------------------------*/

        //Class is like a blueprint
        //see car.java for more notes.



        //"car" is a class I've defined. To initialize this object, type the following.
        car verna = new car();
        car city = new car();

        //To add some details to the object we just created. Just write the object we want to modify, add a dot, and write the pre-defined field.



        //Using the setter
        verna.setDoors(4);



        //Using the getter
        System.out.println(verna.getDoors());

        int x = verna.getDoors();
        System.out.println(x);






        /*-----------------------------
        Getter Setter Challenge
        ------------------------------*/

        //Bank account
/*
        bank alpha_account = new bank();

        alpha_account.setAcn(1);
        alpha_account.setBalance(100);
        alpha_account.setEmail("alpha@gmail.com");
        alpha_account.setName("Alpha Joe");
        alpha_account.setPhone(9999999911L);

        alpha_account.deposit(50);
        alpha_account.withdraw(140);
        alpha_account.withdraw(20);
*/




        /*-----------------------------
        Constructor
        ------------------------------*/

        //Typing alpha_account.set<something> for a lot of fields can be tedious. Use a constructor to streamline the process.

        //Create an overload method inside class.
        //Use code->generate->constructor.

        bank beta_account = new bank(2,2000,"Beta Joe", "beta@gmail.com", 9696969696L);

        beta_account.deposit(500);
        beta_account.withdraw(2499);
        beta_account.withdraw(2);
        System.out.println(beta_account.getBalance());




        /*-----------------------------
        Constructor Challenge
        ------------------------------*/

        //In order to get default values, create an overload method with some defaults.



        //This one uses defaults
        vip vip_1 = new vip();

        //This one has args
        vip vip_2 = new vip("VIP Joe", 1000000, "vip@gmail.com");


        //Checking for correct assignment
        System.out.println(vip_1.getName());
        System.out.println(vip_2.getName());

        //Changing a single field: use setter
        vip_1.setName("VIP Johnson");
        System.out.println(vip_1.getName());


    }
}
