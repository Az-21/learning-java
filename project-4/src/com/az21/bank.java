package com.az21;

public class bank {

    private int acn;
    private double balance;
    private String name;
    private String email;
    private long phone;


    public int getAcn() {
        return acn;
    }

    public void setAcn(int acn) {
        this.acn = acn;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getPhone() {
        return phone;
    }

    public void setPhone(long phone) {
        this.phone = phone;
    }


    public double deposit(double deposit_amount){

        balance = balance + deposit_amount;
        System.out.println(+deposit_amount + " successfully added to your account. Your new balance is " + balance);
        return balance;
    }

    public double withdraw(double withdraw_amount){

        if (balance - withdraw_amount >= 0) {
            balance = balance - withdraw_amount;
            System.out.println(withdraw_amount + " successfully deducted from your account. Your new balance is " + balance);
            return balance;
        }
        else {
            System.out.println("Insufficient funds");
            return balance;
        }
    }


    //Constructor
    public bank(int acn, double balance, String name, String email, long phone) {
        this.acn = acn;
        this.balance = balance;
        this.name = name;
        this.email = email;
        this.phone = phone;
    }
}

