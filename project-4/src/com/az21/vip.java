package com.az21;

public class vip {

    private String name;
    private int credit_limit;
    private String email;


    //Constructor
    public vip(String name, int credit_limit, String email) {
        this.name = name;
        this.credit_limit = credit_limit;
        this.email = email;
    }

    //Constructor overload: defaults
    //This is used when no arguments are provided.
    public vip(){
        this("Default name. Update ASAP", 100, "Default email. Update ASAP");
    }


    //Setter and Getter

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCredit_limit() {
        return credit_limit;
    }

    public void setCredit_limit(int credit_limit) {
        this.credit_limit = credit_limit;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
