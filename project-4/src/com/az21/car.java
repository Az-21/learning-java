package com.az21;

public class car {


    //Create the editable fields of this object
    private int doors;
    private int wheels;
    private String model;
    private String color;


//Since the vars above are private (only accessible in car.java) we need to write one more thing.


    //TIP: Click code->generate->getter and setter to do this automatically



    //setter: use in main to set
    public void setDoors(int doors) {
        this.doors = doors;
    }

    public void setWheels(int wheels) {
        this.wheels = wheels;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setColor(String color) {
        this.color = color;
    }


    //getter: use in main to get
    public int getDoors() {
        return this.doors;
    }

    public int getWheels() {
        return wheels;
    }

    public String getModel() {
        return model;
    }

    public String getColor() {
        return color;
    }


}
