package com.company;

public class Main {

    public static void main(String[] args) {

        /*----------------------------
        Inheritance
        ------------------------------*/

        /*
        * We can create multiple classes
        * We can inherit some properties and variables from one to another.
        * keyword: extends
        * here we inherit animal class to dog class
        *
        * public class animal;
        * public class dog extends animal;
        *
        * click on generate -> constructor
         */

        dog poodle = new dog("Poodle", 3, 2, 2, 4, 1);


        //Although get weight was not defined in dog class, it was inherited from animal superclass.
        int w = poodle.getWeight();
        System.out.println(w);

        //Same is true for methods in superclass.
        poodle.move();

        //We can even override the methods in superclass. Create it using generate->override method
        poodle.eat();

    }
}
