package com.company;

public class animal {

    private String name;
    private int brain;
    private int body;
    private int size;
    private int weight;


    //Constructor
    public animal(String name, int brain, int body, int size, int weight) {
        this.name = name;
        this.brain = brain;
        this.body = body;
        this.size = size;
        this.weight = weight;
    }

    //Setter and Getter
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBrain() {
        return brain;
    }

    public void setBrain(int brain) {
        this.brain = brain;
    }

    public int getBody() {
        return body;
    }

    public void setBody(int body) {
        this.body = body;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }



    //If we add a method here, every inherited class can use it.

    public void move(){
        System.out.println("animal.move was called.");

    }

    public void eat(){
        System.out.println("animal.eat was called.");
    }
}
