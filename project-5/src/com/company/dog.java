package com.company;

public class dog extends animal {


    //Here we can add some more features unique to dogs.
    private int eyes;
    private int legs;
    private int tail;


    /*
    * Make the following using generate -> constructor.
    * It has some initializations like brain, and body, which is true for all dogs. So we'll remove it from constructor of dog, but write the value of it in super which will satisfy the constructor of animal.
    *
    *
    public dog(String name, int brain, int body, int size, int weight) {

        //We can edit some fields which are same for all dogs
        super(name, brain, body, size, weight);

        */

    public dog(String name, int size, int weight, int eyes, int legs, int tail) {
        super(name, 1, 1, size, weight);
        this.eyes = eyes;
        this.legs = legs;
        this.tail = tail;
    }



    //Setter and Getter
    public int getEyes() {
        return eyes;
    }

    public void setEyes(int eyes) {
        this.eyes = eyes;
    }

    public int getLegs() {
        return legs;
    }

    public void setLegs(int legs) {
        this.legs = legs;
    }

    public int getTail() {
        return tail;
    }

    public void setTail(int tail) {
        this.tail = tail;
    }


    //Override the superclass method.
    @Override
    public void eat() {
        System.out.println("animal.eat() was called from dog.java");
        //super.eat();
    }
}
