package com.company;

public class car {

    private int doors;
    private String name;
    private int cost;
    private int gear;
    private int speed;


    //Constructor
    public car(int doors, String name, int cost, int gear, int speed) {
        this.doors = doors;
        this.name = name;
        this.cost = cost;
        this.gear = gear;
        this.speed = speed;
    }



    //Setter and Getter
    public int getDoors() {
        return doors;
    }

    public void setDoors(int doors) {
        this.doors = doors;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public int getGear() {
        return gear;
    }

    public void setGear(int gear) {
        this.gear = gear;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }


    //Method to increase/decrease speed
    public void increase_speed (int delta_speed){
        speed = speed + delta_speed;
        //+ve delta for add
        //-ve delta for deduct
    }


    //Method to change gear
    public void change_gear (int delta_gear){
        gear = gear + delta_gear;
    }




}
