package com.company;

public class activa extends car {


    //Activa's unique vars
    private String variant;
    private int add_ons; // 0/1


    //Modifying such that things which are true for all activas remain constant.

    //Constructor
    public activa(String name, int cost, int speed, String variant, int add_ons) {
        super(0, name, cost, 0, speed);
        this.variant = variant;
        this.add_ons = add_ons;
    }



    //Setter and Getter
    public String getVariant() {
        return variant;
    }

    public void setVariant(String variant) {
        this.variant = variant;
    }

    public int getAdd_ons() {
        return add_ons;
    }

    public void setAdd_ons(int add_ons) {
        this.add_ons = add_ons;
    }
}
