package com.company;

public class hardware {

    //Vars
    private String cpu;
    private int cpu_clock;
    private String gpu;
    private String ram;


    //Constructor
    public hardware(String cpu, int cpu_clock, String gpu, String ram) {
        this.cpu = cpu;
        this.cpu_clock = cpu_clock;
        this.gpu = gpu;
        this.ram = ram;
    }


    //Setter and Getter
    public String getCpu() {
        return cpu;
    }

    public void setCpu(String cpu) {
        this.cpu = cpu;
    }

    public String getGpu() {
        return gpu;
    }

    public void setGpu(String gpu) {
        this.gpu = gpu;
    }

    public String getRam() {
        return ram;
    }

    public void setRam(String ram) {
        this.ram = ram;
    }


    //Method: Overclock/Underclock
    public void overclock(int delta_clock){
        System.out.println("Currently clocked at " + cpu_clock + " GHz");
        this.cpu_clock = this.cpu_clock + delta_clock;

        if (delta_clock<0){
            System.out.println("Underclocked successfully. Your CPU is currently clocked at " + cpu_clock);
        }
        else if (delta_clock>0){
            System.out.println("Overclocked successfully. Your CPU is currently clocked at " + cpu_clock);
        }
        else{
            System.out.println("Please provide a non-zero value");
        }
    }




}



