package com.company;

public class misc {

    //Vars
    private String mouse;
    private int mouse_dpi;
    private String keyboard;
    private String keys;
    private String ex_mic;
    private String controller;


    //Constructor
    public misc(String mouse, int mouse_dpi, String keyboard, String keys, String ex_mic, String controller) {
        this.mouse = mouse;
        this.mouse_dpi = mouse_dpi;
        this.keyboard = keyboard;
        this.keys = keys;
        this.ex_mic = ex_mic;
        this.controller = controller;
    }


    //Setter and Getter
    public String getMouse() {
        return mouse;
    }

    public void setMouse(String mouse) {
        this.mouse = mouse;
    }

    public int getMouse_dpi() {
        return mouse_dpi;
    }

    public void setMouse_dpi(int mouse_dpi) {
        this.mouse_dpi = mouse_dpi;
    }

    public String getKeyboard() {
        return keyboard;
    }

    public void setKeyboard(String keyboard) {
        this.keyboard = keyboard;
    }

    public String getKeys() {
        return keys;
    }

    public void setKeys(String keys) {
        this.keys = keys;
    }

    public String getEx_mic() {
        return ex_mic;
    }

    public void setEx_mic(String ex_mic) {
        this.ex_mic = ex_mic;
    }

    public String getController() {
        return controller;
    }

    public void setController(String controller) {
        this.controller = controller;
    }
}
