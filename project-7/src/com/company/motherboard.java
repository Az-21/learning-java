package com.company;

public class motherboard {

    //Vars
    private String model;
    private int ram_slots;
    private int no_sata;
    private int no_m2;
    private String bios;


    //Constructor
    public motherboard(String model, int ram_slots, int no_sata, int no_m2, String bios) {
        this.model = model;
        this.ram_slots = ram_slots;
        this.no_sata = no_sata;
        this.no_m2 = no_m2;
        this.bios = bios;
    }


    //Setter and Getter
    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getRam_slots() {
        return ram_slots;
    }

    public void setRam_slots(int ram_slots) {
        this.ram_slots = ram_slots;
    }

    public int getNo_sata() {
        return no_sata;
    }

    public void setNo_sata(int no_sata) {
        this.no_sata = no_sata;
    }

    public int getNo_m2() {
        return no_m2;
    }

    public void setNo_m2(int no_m2) {
        this.no_m2 = no_m2;
    }

    public String getBios() {
        return bios;
    }

    public void setBios(String bios) {
        this.bios = bios;
    }
}
