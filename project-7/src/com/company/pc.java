package com.company;

public class pc {

    //Composition
    private motherboard c_motherboard;
    private misc c_misc;
    private hardware c_hardware;


    //Constructor
    public pc(motherboard c_motherboard, misc c_misc, hardware c_hardware) {
        this.c_motherboard = c_motherboard;
        this.c_misc = c_misc;
        this.c_hardware = c_hardware;
    }

    //Setter and Getter
    public motherboard getC_motherboard() {
        return c_motherboard;
    }

    public void setC_motherboard(motherboard c_motherboard) {
        this.c_motherboard = c_motherboard;
    }

    public misc getC_misc() {
        return c_misc;
    }

    public void setC_misc(misc c_misc) {
        this.c_misc = c_misc;
    }

    public hardware getC_hardware() {
        return c_hardware;
    }

    public void setC_hardware(hardware c_hardware) {
        this.c_hardware = c_hardware;
    }
}
