package com.company;

public class Main {

    public static void main(String[] args) {

        //Inheritance is limited to only 1 class. An alternative is composition.

        /*
         * in this project
         * hardware, motherboard, and misc are 3 classes
         * pc is a composite class
         */

        hardware firefly_hardware = new hardware("Ryzen Threadripper", 3, "GTX 1080", "Generic RAM 5000");

        misc firefly_misc = new misc("Razor Death Adder", 10_000, "Razer Chroma", "Cherry MX", "Yeti", "PS4 Controller v2");

        motherboard firefly_motherboard = new motherboard("ASUS ROG Firefly", 8, 2, 4, "UEFI");


        //Using the composition
        pc firefly = new pc(firefly_motherboard, firefly_misc, firefly_hardware);

        //Now pc can be used as an aggregator for the lower level methods.

        firefly.getC_hardware().overclock(1);
        firefly.getC_hardware().overclock(-2);

    }
}
