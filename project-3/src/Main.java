public class Main {

    public static void main(String[] args) {

        /*----------------------
        Switch Statement
         -----------------------*/

        //Switch is essentially an if-elseif statement which is performed in a case by case statement.
        //It is a preference thing

        int x = 7;

        switch (x) {
            case 1:
                System.out.println("x = 1");
                break;

            case 2:
                System.out.println("x = 2");
                break;

            case 3:
                System.out.println("x = 3");
                break;

            case 4:
            case 5:
            case 6: //= if (x=4,5,6)
                System.out.println("x = 4, 5, or 6.");
                break;

            default: //use default like "else"
                System.out.println("x = U -[1,6]");
        }


        /*----------------------
        Switch Statement Challenge
         -----------------------*/

        char alpha = 'a';

        switch (alpha) {

            case 'a':
                System.out.println("a");
                break;

            case 'A':
                System.out.println("A");
                break;

            case 'b':
                System.out.println("b");
                break;

            case 'B':
                System.out.println("B");
                break;

            default:
                System.out.println("No grade found");
                break;
        }



        /*----------------------
        For Loop (Compound Interest)
         -----------------------*/


        // for (first; last; increment){code}
        for (int index = 1; index < 9; index = index + 1) {
            //     i  ;loop until it is false; ++

            System.out.println("10,000 interest at " + index + "% interest = " + calculate_interest(10000, index));
        }


        /*----------------------
        Change Double Precision
         -----------------------*/

        //String.format("%.2f", <var,method,or calc here>)
        double div_1 = (double) 10 / 3;

        System.out.println(String.format("%.2f", div_1));
        System.out.println(String.format("%.3f", div_1));




        /*----------------------
        For loop challenge (prime number bool)
        See end for method
         -----------------------*/

        for (int nat = 0; nat < 10; nat = nat + 1) {

            if (is_prime(nat) == true) {

                System.out.println(nat + " is a prime number");

            }
        }


        /*----------------------
        While loop
         -----------------------*/

        //while(<condition becomes false>){code}

        int num_1 = 0;
        while (num_1 < 5) {
            System.out.println(num_1);
            num_1 = num_1 + 1;
        }


        //alternative

        System.out.println("\n");

        num_1 = 0;
        while (true) {
            if (num_1 == 5) {
                break;
            } else {
                System.out.println(num_1);
                num_1 = num_1 + 1;
            }
        }



        /*----------------------
        Even number detection challenge
         -----------------------*/

        for (int i = 0; i <=100; i = i + 1){
            System.out.println(i + " is an even number: " + is_even(i));
        }






    }

    public static double calculate_interest(double principle, double interest_rate) {

        double principle_plus_interest = principle + principle * interest_rate / 100;
        return principle_plus_interest;
    }

    public static boolean is_prime(int n) {

        if (n == 1) {
            return false;
        } else if (n == 0) {
            return false;
        } else if (n > 1) {

            for (int i = 2; i < n; i = i + 1) {
                if (n % i == 0) {
                    return false;

                    /*Super IMP!!!
                    return automatically breaks the loop.
                     */
                }


            }
        }
        return true;
    }


    public static boolean is_even(int number){

        if (number == 0){
            return true;
        }
        else if (number % 2 == 0){
            return true;
        }
        else {
            return false;
        }

    }

}



