public class Expression_statements_and_methods {

    public static void main(String[] args) {

        /*--------------------------------
        Expressions
        ----------------------------------*/

        //Expression is anything with value, or assigned value.

        int number_1 = 100;
        //Here "number_1 = 100" is an expression

        if (number_1 > 99) {
            System.out.println("Something");
        }
        //Here "number_1 > 99" is an expression
        //"Something" is also an expression.




        /*--------------------------------
        Keywords
        ----------------------------------*/

        //Keywords are reserved names.
        //Example: int, long, String, if, +\approx 50 others




        /*--------------------------------
        Statement
        ----------------------------------*/

        //Every value assignment, edit, and method calls are statements.
        //Statements can span multiple lines.
        //Basically anything between 2 consecutive semicolons is a statement.




        /*--------------------------------
        Code blocks [if-else if-else]
        ----------------------------------*/

        int number_2 = 200;

        if (number_2 == 400) {
            System.out.println("Number = 400");
        } else if (number_2 == 300) {
            System.out.println("Number = 300");
        } else {
            System.out.println("Number = 200");
        }

        //Important thing about code blocks is that we cannot access the variables created in them. They are created and destroyed in the same block. Here sout is a code block




        /*--------------------------------
        Game Over Score Calc
        ----------------------------------*/

        int score = 10000;
        int level_reached = 8;
        int bonus = 200;

        int score_plus_bonus = score + (level_reached * bonus); //This was necessary because a code block cannot retain this value. It does find the value, but it is removed from memory as soon as it is completed.

        System.out.println("Score: " + score);
        System.out.println("Levels completed: " + level_reached);
        System.out.println("Level bonus: " + bonus + "\n");
        System.out.println("FINAL SCORE: " + score_plus_bonus);
        System.out.println("\n");






        /*--------------------------------
        Methods in use. See the end of doc to see more
        ----------------------------------*/


        calculate_game_score(); //See method notes at the very end of doc.
        System.out.println("\n");


        calculate_game_score_args(20000, 8, 200); //See method notes at the very end.
        System.out.println("\n");




        /*--------------------------------
        Method with a return value
        ----------------------------------*/

        //Use the return data type instead of void to return a value.

        return_score(20000, 8, 200);
        //This code wasn't used because we didn't place any output in the method except for return int.

        score_plus_bonus = return_score(20000, 8, 200);
        System.out.println("Final score: " + score_plus_bonus);



        /*--------------------------------
        High Score Challenge//See methods at end
        ----------------------------------*/

        String name = "Akshat";
        score_plus_bonus = return_score(20000, 8, 200);
        int position = highscore_position(score_plus_bonus);

        System.out.println("Game Over. " + name + " scored " + score_plus_bonus + " points. Your grade: " + position);



        /*--------------------------------
        Method Overloading
        ----------------------------------*/

        //Basically using the same name of method over and over, but with different arguments.

        //Eg: method_1(int x, String y)  {Code}
        //    method_1(int x)   {Code}
        //    method_1(String y, int x)  {Code}

        //Basically, what makes them unique is the number and order of data types.


    }

    /*--------------------------------
    Method
    ----------------------------------*/

    //Method is basically a function. We cannot place a method within a method.

    public static void calculate_game_score() {

        int score = 10000;
        int level_reached = 4;
        int bonus = 100;
        int score_plus_bonus = score + (level_reached * bonus);

        System.out.println("FINAL SCORE: " + score_plus_bonus);


        //Now this method can be called anywhere. Even in the methods above this particular method.

        //To call this method, just type name of method and provide arguments if it has them.
    }





    /*Method with arguments*/

    public static void calculate_game_score_args(int score, int level_reached, int bonus) {

        int score_plus_bonus = score + (level_reached * bonus);
        System.out.println("FINAL SCORE: " + score_plus_bonus);


        //Now this method can be called anywhere. Even in the methods above this particular method.

        //To call this method, just type name of method and provide arguments if it has them.
    }





    //Method with return

    public static int return_score(int score, int level_reached, int bonus) {
        int score_plus_bonus = score + (level_reached * bonus);

        return score_plus_bonus;
    }





    //High score challenge

    public static int highscore_position (int score_plus_bonus) {

        if (score_plus_bonus >= 1000){
            return 1;
        }
        else if (score_plus_bonus<1000 && score_plus_bonus >= 500){
            return 2;
        }
        else if (score_plus_bonus<500 && score_plus_bonus>=100){
            return 3;
        }
        else{
            return 4;
        }

    }



}
