//This is a class. Classes are sub-programs in the main program.
public class Hello_world {

    //Initializing the main
    public static void main(String[] args) {

        //Print syntax
        System.out.println("Hello World");
        //Type "sout" and then press tab


        //Variables
        //data_type name_of_var = assign_value;

        int number_1 = 5;
        System.out.println(number_1);

        int number_2 = 21;
        System.out.println(number_2);

        //we can also input math expression as a var
        int number_3 = 5 * (2 + 2) - 10;
        System.out.println(number_3);

        //we can also perform external calculations
        int number_4 = number_1 + number_2 + number_3;
        System.out.println(number_4);

        //we can also perform calculations under a function
        System.out.println(number_1 + number_2 + number_3);




        /* ---------------------------------------
        Byte Short Int Long
        ------------------------------------------ */

        //we can use underscore in long numbers to make them easier to read
        int long_num_1 = 1234567;
        int long_num_2 = 1_234_567;


        //range of int
        int min_int = -2_147_483_648;
        int max_int = 2_147_483_647;


        //range of byte (much more efficient)
        byte min_byte = -128;
        byte max_byte = 127;


        //range of short
        short min_short = -32_768;
        short max_short = 32_767;


        //range of long: (-2^64, 2^64)
        //It is important to add "l" or "L" at the end of long var.
        long long_number = 1_000_000L;


//NOTE: It is not possible to perform inter-data-type calculations.
//Use (byte), (short), (int), (long) infront of the incompatible data type to convert them for that line. This keeps the original unaffected.




        /* ---------------------------------------
        Float and Double
        ------------------------------------------ */

        //Float = single precision (add f/F/(float))
        //Double = double precision (add d/D/(double))

        int x_1 = 10 / 3;
        float x_2 = 10F / 3F;
        double x_3 = (double) 10 / 3; //double is preferred

        System.out.println(x_1);
        System.out.println(x_2);
        System.out.println(x_3);



        /* ---------------------------------------
        Char and Bool
        ------------------------------------------ */

        //char: only 1 character is allowed. Unicode chars are also allowed
        char character_1 = 'a';
        char character_2 = 'A';
        char character_3 = '1';
        char character_4 = '\u00A9';
        //unicode is indicated by "backslash u" followed by the actual unicode

        System.out.println(character_1);
        System.out.println(character_2);
        System.out.println(character_3);
        System.out.println(character_4);


        //boolean: true or false
        boolean condition_1 = true;
        boolean condition_2 = false;

        System.out.println(condition_1);
        System.out.println(condition_2);




        /* ---------------------------------------
        String
        ------------------------------------------ */

        //strings have uppercase S
        String string_1 = "This is a string.";
        System.out.println(string_1);

        //we can add string
        String string_2 = " This is another one";
        String string_3 = string_1 + string_2;
        System.out.println(string_3);
        System.out.println(string_1 + string_2);
        System.out.println(string_1 + " This was added in a println command." + string_2);

        //Surprisingly, java can automatically convert numbers to strings.
        int alpha = 10;
        String beta = "50";
        String gamma = alpha + beta;

        System.out.println(gamma + "\n");
        //NOTE: gamma is actually a string. Not a number





        /* ---------------------------------------
        Operators
        ------------------------------------------ */

        //Standard stuff

        int o_number_1 = 7+3;
        int o_number_2 = 7-3;
        int o_number_3 = 7/3;
        double o_number_4 = (double) 7/3;
        int o_number_5 = 7*3;
        int o_number_6 = 7%3; //remainder is %

        System.out.println(o_number_1);
        System.out.println(o_number_2);
        System.out.println(o_number_3);
        System.out.println(o_number_4);
        System.out.println(o_number_5);
        System.out.println(o_number_6);




        /* ---------------------------------------
        If Statement
        ------------------------------------------ */

        if (o_number_1 == 10) {
            System.out.println("Correct");
            o_number_1 = o_number_1 + 1;
        }

        if (o_number_1 != 10) {
            System.out.println("Incorrect");
        }


        int delta = 100;
        int phi = 200;


        if (delta > 99) {
            System.out.println("delta is greater than 99");
        }

        if (delta < 101) {
            System.out.println("delta is less than 101");
        }

        if ((delta == 100) && (phi == 200)){
            System.out.println("Both conditions are true");
        }

        if((delta == 100) || (phi == 200)){
            System.out.println("(1)At least 1 of the condition is true");
        }

        if((delta == 900) || (phi == 200)){
            System.out.println("(2)At least 1 of the condition is true");
        }

        if((delta == 100) || (phi == 900)){
            System.out.println("(3)At least 1 of the condition is true");
        }

        if((delta == 200) || (phi == 300)){
            System.out.println("(4)At least 1 of the condition is true");
        }








    }

}


